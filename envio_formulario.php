<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Coletando dados do formulário
    $nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $assunto = filter_input(INPUT_POST, 'assunto', FILTER_SANITIZE_STRING);
    $mensagem = filter_input(INPUT_POST, 'mensagem', FILTER_SANITIZE_STRING);
    $telefone = filter_input(INPUT_POST, 'telefone', FILTER_SANITIZE_STRING);

    // Validação básica
    if ($nome && $email && $assunto && $mensagem) {
        // Destinatário do e-mail
        $to = "contato@mksistemasbiomedicos.com.br";
        // Assunto do e-mail
        $subject = "Formulário de Contato: " . $assunto;

        // Corpo do e-mail
        $body = "Nome: " . $nome . "\n";
        $body .= "E-mail: " . $email . "\n";
        $body .= "Assunto: " . $assunto . "\n\n";
        $body .= "Mensagem: \n" . $mensagem . "\n Contato: \n" . $telefone . "\n";

        // Cabeçalhos do e-mail
        $headers = "From: " . $email . "\r\n";
        $headers .= "Reply-To: " . $email . "\r\n";
        $headers .= "X-Mailer: PHP/" . phpversion();

        // Enviar e-mail
        if (mail($to, $subject, $body, $headers)) {
            echo "<script>alert('Sua mensagem foi enviada com sucesso!');</script>";
        } else {
            echo "<script>alert('Erro ao enviar mensagem. Tente novamente.');</script>";
        }
    } else {
        echo "<script>alert('Por favor, preencha todos os campos.');</script>";
    }
}
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contato | MK Sistemas Biomédicos</title>

    <!-- SEO Optimization -->
    <meta name="description" content="Entre em contato com a MK Sistemas Biomédicos para saber mais sobre nossas soluções de gestão hospitalar e engenharia clínica.">
    <meta name="keywords" content="contato, MK Sistemas Biomédicos, suporte, atendimento, soluções hospitalares">
    <meta name="robots" content="index, follow">

    <!-- Open Graph (OG) Tags for Social Sharing -->
    <meta property="og:title" content="Contato | MK Sistemas Biomédicos">
    <meta property="og:description" content="Entre em contato com a equipe da MK Sistemas Biomédicos para esclarecer dúvidas e solicitar informações sobre nossos produtos.">
    <meta property="og:image" content="https://www.mksistemasbiomedicos.com.br/img/contato.jpg">
    <meta property="og:url" content="https://www.mksistemasbiomedicos.com.br/contato.html">
    <meta property="og:type" content="website">

    <!-- Structured Data (JSON-LD) -->
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "MK Sistemas Biomédicos",
            "url": "https://www.mksistemasbiomedicos.com.br",
            "logo": "https://www.mksistemasbiomedicos.com.br/logo.png",
            "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "+55-11-1234-5678",
                "contactType": "Customer Service",
                "email": "contato@mksistemasbiomedicos.com.br"
            }
        }
    </script>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootswatch@5.3.0/dist/lux/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet" />
</head>

<body>
    <header class="bg-dark text-light p-3">
        <div class="container">
            <img src="logo.png" alt="Logo MK Sistemas Biomédicos" style="height: 50px; margin-right: 15px;">
            <h1 class="display-4">Entre em Contato</h1>
        </div>
    </header>

    <main class="container mt-4">
        <section>
            <h2>Fale Conosco</h2>
            <p>Preencha o formulário abaixo para enviar sua mensagem. Estamos prontos para ajudar com qualquer dúvida ou solicitação.</p>

            <!-- Contact Form -->
            <form action="envio_formulario.php" method="POST" class="row g-3">
                <div class="col-md-6">
                    <label for="nome" class="form-label">Nome Completo</label>
                    <input type="text" class="form-control" id="nome" name="nome" required>
                </div>
                <div class="col-md-6">
                    <label for="email" class="form-label">E-mail</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
                <div class="col-12">
                    <label for="assunto" class="form-label">Assunto</label>
                    <input type="text" class="form-control" id="assunto" name="assunto" required>
                </div>
                <div class="col-12">
                    <label for="mensagem" class="form-label">Mensagem</label>
                    <textarea class="form-control" id="mensagem" name="mensagem" rows="5" required></textarea>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </section>

        <section class="mt-5">
            <h2>Outros Meios de Contato</h2>
            <ul>
                <li><strong>E-mail:</strong> <a href="mailto:contato@mksistemasbiomedicos.com.br">contato@mksistemasbiomedicos.com.br</a></li>
            </ul>
        </section>

        <section class="mt-5">
            <h2>Redes Sociais</h2>
            <p>Nos acompanhe nas redes sociais para ficar por dentro das novidades e atualizações.</p>
            <ul class="list-unstyled">
                <li><a href="https://www.linkedin.com/company/mksistemasbiomedicos" target="_blank"><i class="bi bi-linkedin"></i> LinkedIn</a></li>
                <li><a href="https://www.instagram.com/mksistemasbiomedicos/" target="_blank"><i class="bi bi-instagram"></i> Instagram</a></li>
                <li><a href="https://www.facebook.com/mksistemasbiomedicos" target="_blank"><i class="bi bi-facebook"></i> Facebook</a></li>
            </ul>
        </section>
    </main>

    <footer class="bg-dark text-light text-center py-4">
        <p>&copy; 2024 MK Sistemas Biomédicos. Todos os direitos reservados.</p>
        <a href="politica_privacidade.html" class="text-light">Política de Privacidade</a> |
        <a href="termos_servico.html" class="text-light">Termos de Serviço</a>
    </footer>

    <!-- jQuery -->
    <script src="https://www.seth.mksistemasbiomedicos.com.br/framework/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Boots
